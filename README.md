# Fallout-TTRPG (fallout-ttrpg)

Arcane Arcade's custom Fallout TTRPG helper

[View Demo](https://imgur.com/D0ZnpQQ "Demo")

[Win Download](https://drive.google.com/file/d/1ZnnYO7ZeR5BNWTzDTb-udcRohxWJyQFV/view?usp=sharing)

[Linux Download](https://drive.google.com/file/d/1xVMqcjW_t63CufmjzHEzl4Qf2Ikp9cMz/view?usp=sharing)

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
yarn dev
```

### Build the app for production
1. run one of the below
```bash
yarn buildWin
# or
yarn buildLinux
```

2. take a copy of the `libs` folder from the project folder

3. navigate to the "packaged" code in the project's dist folder

4. paste the libs folder in the compiled project's base folder (e.g. `Fallout-TTRPG-win32-x64`)

5. move your project to wherever on your hard drive and run!

#### Run on Linux

```bash
# in project directory
./Fallout-TTRPG
```

#### Run on Windows

1. Open the project folder and double click on the .exe
2. if your windows defender warns you about the program, click "more information" and then "run anyway"

### Customize the data

All data used by this project can be found in the `libs` folder. **Edit at your own risk! Always back up your project before attempting changes!**
