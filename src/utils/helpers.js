const { NUMBER_FIELDS } = window.$fallout.requestData('constants')

function isNumberField(k) {
  return !!NUMBER_FIELDS.find(it => it === k)
}

export {
  isNumberField,
}
