import { defineStore } from 'pinia';

export const useCharacterSheet = defineStore('characterSheet', {
  state: () => ({
    XP: 0,
    name: '',
    race: '',
    background: '',
    level: 1,
    job: '',
    personalityTrait: '',
    ideal: '',
    bond: '',
    flaw: '',
    body: {
      head: '',
      rightArm: '',
      leftArm: '',
      leftLeg: '',
      rightLeg: '',
    },
    carryLoad: 0,
    caps: 0,
    type: 'STANDARD',
    special: {
      strength: 5,
      perception: 5,
      endurance: 5,
      charisma: 5,
      intelligence: 5,
      agility: 5,
      luck: 5,
    },
    skills: {
      guns: 0,
      energyWeapons: 0,
      explosives: 0,
      meleeWeapons: 0,
      unarmed: 0,
      medicine: 0,
      lockpick: 0,
      crafting: 0,
      science: 0,
      sneak: 0,
      survival: 0,
      barter: 0,
      persuasion: 0,
      deception: 0,
      intimidation: 0,
    },
    attributes: {
      armorClass: 0,
      damageThreshold: 0,
      decayThreshold: {
        max: 0,
        current: 0
      },
      decayLevel: 0,
      staminaPoints: {
        max: 0,
        current: 0
      },
      hitPoints: {
        max: 0,
        current: 0
      },
      healingRate: {
        max: 0,
        current: 0
      },
      actionPoints: 0,
      passivePerception: 0,
    },
    weapons: [],
    armors: [],
    equipment: [],
    misc: [],
    conditions: [],
    ammo: [],
    hunger: 0,
    thirst: 0,
    fatigue: 0,
    radResistance: 0,
    radLevel: 0,
    rads: 0,
    traits: [{ _uid: 0, value: '' }],
    perks: [{ _uid: 0, value: '' }],
    notes: [{ _uid: 0, value: '' }],
    importantPeople: [{ _uid: 0, value: '' }],
    places: [{ _uid: 0, value: '' }],
  }),
  getters: {
    heldArmor: (state) => state.armors,
    heldWeapons: (state) => state.weapons,
    calculatedLoad: (state) => {
      return state.weapons.concat(
        state.armors.concat(
          state.misc.concat(
            state.ammo.concat(
              state.equipment
            )
          )
        )
      ).reduce((i, it) => {
        const wearLoad = parseFloat(it.wearLoad) || -1
        const quantity = parseInt(it.quantity)
        const packSize = parseInt(it.packSize)
        let carryLoad = isNaN(packSize) ? parseFloat(it.carryLoad) : 1 / packSize
        const load = wearLoad > -1 && it.equipped ? wearLoad : (carryLoad || 0)
        return i += Math.floor((load * (isNaN(quantity) ? 1 : quantity)) || 0)
      }, 0)
    }
  },
  // actions: {
  //   increment() {
  //     this.counter++;
  //   },
  // },
});
