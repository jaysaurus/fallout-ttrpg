import { app, BrowserWindow, Menu, nativeTheme } from 'electron'
import path from 'path'
import os from 'os'
import { statSync, readFileSync } from 'fs'

// needed in case process is undefined under Linux
const platform = process.platform || os.platform()

try {
  if (platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(path.join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }

let mainWindow
let splash

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    icon: path.resolve(__dirname, 'icons/icon.png'), // tray icon
    width: 1000,
    height: 600,
    useContentSize: true,
    fullscreen: true,
    alwaysOnTop: false,
    webPreferences: {
      contextIsolation: true,
      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD)
    }
  })

  splash = new BrowserWindow({
    width: 1024,
    height: 768,
    transparent: true,
    frame: false,
    alwaysOnTop: true
  })


  mainWindow.loadURL(process.env.APP_URL)

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools()
  } else {
    // we're on production; no access to devtools pls
    splash.loadURL(process.env.APP_URL.replace('index.html', 'splash.html'))
    mainWindow.webContents.on('devtools-opened', () => {
      mainWindow.webContents.closeDevTools()
    })
  }

  const menu = Menu.buildFromTemplate([])
  Menu.setApplicationMenu(menu)

  mainWindow.once('ready-to-show', () => {
    setTimeout(() => {
      if (!process.env.DEBUGGING) splash.destroy()
      mainWindow.show()
    }, 1000)
  })

  mainWindow.on('closed', () => {
    mainWindow = null
    app.quit()
  })
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }

})
