/**
 * This file is used specifically for security reasons.
 * Here you can access Nodejs stuff and inject functionality into
 * the renderer thread (accessible there through the "window" object)
 *
 * WARNING!
 * If you import anything from node_modules, then make sure that the package is specified
 * in package.json > dependencies and NOT in devDependencies
 *
 * Example (injects window.myAPI.doAThing() into renderer thread):
 *
 *   import { contextBridge } from 'electron'
 *
 *   contextBridge.exposeInMainWorld('myAPI', {
 *     doAThing: () => {}
 *   })
 */
import { contextBridge } from 'electron'
import { readFileSync, promises } from 'fs'

contextBridge.exposeInMainWorld('$fallout', {
  requestCharacterSheet: () => {
    try {
      const file = readFileSync(
        process.cwd() + '/libs/characterSheet.json',
        'utf-8')
      return file.toString()
    } catch (e) {
      return null
    }
  },

  requestData: (address) => {
    const file = readFileSync(
      process.cwd() + '/libs/data/' + address.replace(/\s|\||\*|\./g, '') + '.json',
      'utf-8')
    return JSON.parse(file.toString())
  },

  saveCharacterSheet: async (data) => {
    try {
      if (typeof data === 'string') {
        await promises.writeFile(process.cwd() + '/libs/characterSheet.json', data);
        return { success: true }
      }
    } catch (error) {
      return { success: false, error }
    }
  }
})
